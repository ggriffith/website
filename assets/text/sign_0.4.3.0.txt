{ 'files': { 'eccoin-0.4.3.0-arm32.tar.gz': '94625c829400030a849e0b659f6f61dc9fd19225eb3d430a4dc26aa269f76926',
             'eccoin-0.4.3.0-arm64.tar.gz': '537063efc8502cb5d7206252fca299195af051e1b45b762caa0d00fc49ac088b',
             'eccoin-0.4.3.0-linux32.tar.gz': '6027a36189031e733966b3cf8ac4617d64e68871e3e5bd4365a87a618f5d923c',
             'eccoin-0.4.3.0-linux64.tar.gz': '7892515508dc273748ab7ab3554703fd18b75e01acb345decac7b8ac94c67f54',
             'eccoin-0.4.3.0-osx64.tar.gz': 'f604dfa9091cb9ac5dd7bca51b973682ca1780dc88f66a586752c9072d1aff17',
             'eccoin-0.4.3.0-win32.zip': 'd3f56c8b2deee85acafce39dfbda730aeb76f20b6251d595a59e5c02a06e8838',
             'eccoin-0.4.3.0-win64.zip': '91e3955b0d6ac7abed07715f0452e533570793afc78c97d491e92a35b52bb09b'},
  'program': 'eccoin',
  'version': '0.4.3.0'}
